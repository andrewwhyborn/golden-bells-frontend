import React from 'react';
import {Link} from "react-router-dom"
import {ReactComponent as BellIcon} from "../../assets/images/bell.svg"
import {ReactComponent as TimeIcon} from "../../assets/images/time.svg"
import {ReactComponent as MainIcon} from "../../assets/images/main.svg"

const Settings = () => {
  return (
    <div className="main-area-field full-height">
      <div className="main-area full-height">
        <div className="page-area no-p">
          <div className="title-block">
            <div className="title">Настройки</div>
          </div>
          <div className="content">
            <div className="block setting-block">
              <Link to="/" className="setting-link">
                <div className="icon-area">
                  <BellIcon className="setting-icon"/>
                </div>

                <div className="setting-text">Сила удара</div>
              </Link>
              <Link to="/" className="setting-link">
                <div className="icon-area">
                  <TimeIcon className="setting-icon icon-time"/>
                </div>
                <div className="setting-text">Длительность удара</div>
              </Link>
              <Link to="/" className="setting-link">
                <div className="icon-area">
                  <MainIcon className="setting-icon"/>
                </div>
                <div className="setting-text">Основные</div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Settings;
