import axios from 'axios'

export default {
    signup : (data) => axios.post('/users/signup', data)
}