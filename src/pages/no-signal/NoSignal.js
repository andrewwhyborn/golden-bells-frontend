import React from 'react';
import { ReactComponent as NoSignalIcon } from "../../assets/images/no-signal.svg"

const NoSignal = () => {
  return (
    <div className="main-area-field full-height no-signal-area">
      <div className="main-area full-height">
        <div className="page-area center-h _no-signal">
          <NoSignalIcon/>
          <div className="signal-info">
            Последний выход на связь
            <br/><span>15:16 17.02.2021</span>
          </div>
          <div className="signal-bold">
            <span>Нет соединения</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NoSignal;
