import axios from 'axios'

export default {
    reset : (data) => axios.post('/users/reset', data),
    resetConfirm: (data) => axios.post('/users/reset-confirm', data)
}