import axios from 'axios'

export default {
    feedback: (data) => axios.post('/feedback', data),
}