import React from 'react';

const About = () => {
  return (
    <div className="container-area-field about-color" id="about">
      <div className="container-main-area">
        <div className="about-area">
          <div className="title-home title-block">
            <h2 className="title">
              О нас
            </h2>
          </div>
          <div className="title-home title-block">
            <p className="title-sub">
              Нами разработан и произведен электронный помощник «Золотой звон»
            </p>
          </div>
          <div className="about-info">
            <p>
              Более 15 лет опыта в области автоматики и программирования позволили нашей команде, совместно с профессиональными звонарями, разработать функционального и доступного помощника.
              Данное оборудование повышает удобство совершения церковных таинств и богослужений.
              Настоятель (служитель) храма или монастыря в любой момент из любой точки мира может включить колокольный звон с помощью нашей системы. При этом сохраняется возможность ручного звона.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
