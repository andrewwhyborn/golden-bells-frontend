import React, { useContext, useState, useEffect } from "react";
import { StateContext } from "../../../context/StateContext";
import Loading from "../../../components/loading/loading";
import Logs from "./Logs";
import Header from "../../../components/header/header";

const ImpactForce = () => {
  const STATE = useContext(StateContext);
  const [bells, setBells] = useState([]);

  const changePower = (index, move) => {
    let newBells = [...bells];
    const power = newBells[index].power;

    if (move === "dec") {
      if (power > 0) {
        newBells[index].power = newBells[index].power - 1;
      }
    } else {
      if (power < 255) {
        newBells[index].power = newBells[index].power + 1;
      }
    }

    setBells(newBells);
  };

  const bellsList = bells.map((bell, index) => {
    if (index < STATE.lastData.settings.count) {
      return (
        <div className="force-item" key={index}>
          <div className="force-num">№ {index + 1}</div>
          <div className="force-count-area">
            <div className="dec" onClick={() => changePower(index, "dec")}>
              -
            </div>
            <div className="count">{bell.power}</div>
            <div className="inc" onClick={() => changePower(index, "inc")}>
              +
            </div>
          </div>
        </div>
      );
    } else return "";
  });

  useEffect(() => {
    setBells(STATE.lastData.bells);
  }, []);

  useEffect(() => {
    const newBells = [];

    STATE.lastData.bells.forEach((bell) => {
      newBells.push({ power: bell.power, time: bell.time });
    });
    setBells(newBells);
  }, []);

  const save = (e) => {
    STATE.sendData({ bells });
  };

  return (
    <div className="main-area-field full-height">
      <div className="main-area full-height">
        <div className="page-area no-p">
          <Header
            title={"Сила удара"}
            backTitle="Настройки"
            backUrl={`/device/${STATE.subscribed.id}/settings`}
          />

          <Logs />

          <div className="page-area center-h">
            <div className="content">
              <div className="block force-block">
                <div className="force-list">{bellsList}</div>
              </div>
              <div className="button-area _center">
                <button
                  className="btn btn-yellow chimes-save"
                  onClick={(e) => save(e)}
                >
                  {!STATE.isSending && "СОХРАНИТЬ"}
                  {STATE.isSending && <Loading />}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImpactForce;
