import axios from 'axios'

export default {
    login : (data) => axios.post('/users/login', data),
    me : () => axios.get('/users/me')
}