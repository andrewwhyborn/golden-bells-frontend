import React from 'react'
import {Link} from "react-router-dom"

const LayoutPage = () => {
  return (
    <div className="main-area-field">
      <div className="main-area" style={{flexDirection: "column",display: 'flex'}}>
        <Link to="/">Лендос</Link>
        <Link to="1">Вход-регистрация</Link>
        <Link to="/login">Вход в личный кабинет</Link>
        <Link to="/signup">Регистрация</Link>
        <Link to="5">Нет соединения</Link>
        <Link to="6">Соединение установлено</Link>
        <Link to="7">Куранты</Link>
        <Link to="9">Настройки</Link>
        <Link to="10">Сила удара</Link>
        <Link to="11">Длительность удара</Link>
        <Link to="12">Общее</Link>
        <Link to="13">Звоны</Link>
        <Link to="15">Таймеры</Link>
        <Link to="14">Элементы</Link>
      </div>
    </div>
  )
}

export default LayoutPage
