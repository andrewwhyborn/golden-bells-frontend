import React, { useState } from 'react'
import { ReactComponent as SelectIcon } from "../../assets/images/select-icon.svg"

const Select = ({ options, event, selected = 0 }) => {

  const [open, setOpen] = useState(false)

  let optionsList = options.map((option, index) => {
    return (
      <span
        key={index}
        onClick={() => click(option)}
        style={(selected !== null && parseInt(selected, 10) === parseInt(option, 10)) ? { fontWeight: 700 } : {}}
      >
        {option}</span>
    )
  })

  const click = (option) => {
    event(+option)
    setOpen(false)
  }
  return (
    <div className={"custom-select-area mini" + (open ? " _open" : "")}>
      <div
        onClick={() => setOpen(!open)}
        className="custom-select">
        <span>{`${selected.toString().length > 1 ? selected : `0${selected}`}`}</span>
        <SelectIcon className="select-icon" />
      </div>
      <div className="custom-select-result">
        <div className="custom-select-options">
          {optionsList}
        </div>
      </div>
    </div>
  )
}

export default Select
